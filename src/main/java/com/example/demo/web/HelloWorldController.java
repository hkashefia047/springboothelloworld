package com.example.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    //@RequestMapping("/hello")
    @GetMapping("/hello")
    public String sayHello(@RequestParam("username")String username ,@RequestParam("password")String password){
        return " usermame: "+ username + " password: "+ password;
    }

}
